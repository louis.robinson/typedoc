/**
 * Preprocesses a phone number for use in a LiveRamp ATS envelope. See: https://developers.liveramp.com/authenticatedtraffic-api/docs/3-prepare-the-identifiers.
 * - Removes leading 1s (US country code), including any leading plus sign (+)
 * - Removes parentheses, dashes, periods, and spaces
 *
 * @param {string} phoneNumber - The phone number to be preprocessed.
 * @returns {string} - The preprocessed phone number address.
 */
export function preProcessPhoneNumber (phoneNumber: string): string {
  return phoneNumber.replace('someString', '')
}

export function nonAnnotatedStringParse(value: string): string {
  return value.replace('a', 'b')
}

function nonExportedTest(): number {
  return 6
}
