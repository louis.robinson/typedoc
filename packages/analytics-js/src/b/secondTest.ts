
/**
 * Checks if the OptOutGroup contains opt out categories that would prevent the eCST flow
 *
 * @param cookie - The OptanonConsent cookie string.
 * @returns {boolean} - eCST flow is enabled or disabled
 */
export function cookieConsentToEcstFlow (cookie: string): boolean {
  const optOutGroups: Array<string> = []

  return !(optOutGroups.includes('C0004') || optOutGroups.includes('C0005'))
}

export function blahBlah(): boolean {
  return false
}
