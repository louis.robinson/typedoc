[typedoc-test](../README.md) / [Exports](../modules.md) / b/secondTest

# Module: b/secondTest

## Table of contents

### Functions

- [cookieConsentToEcstFlow](b_secondTest.md#cookieconsenttoecstflow)

## Functions

### cookieConsentToEcstFlow

▸ **cookieConsentToEcstFlow**(`cookie`): `boolean`

Checks if the OptOutGroup contains opt out categories that would prevent the eCST flow

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `cookie` | `string` | The OptanonConsent cookie string. |

#### Returns

`boolean`

- eCST flow is enabled or disabled

#### Defined in

[b/secondTest.ts:8](https://gitlab.com/louis.robinson/typedoc/-/blob/1dcf5dd/packages/analytics-js/src/b/secondTest.ts#L8)
