[typedoc-test](../README.md) / [Exports](../modules.md) / a/firstTest

# Module: a/firstTest

## Table of contents

### Functions

- [nonAnnotatedStringParse](a_firstTest.md#nonannotatedstringparse)
- [preProcessPhoneNumber](a_firstTest.md#preprocessphonenumber)

## Functions

### nonAnnotatedStringParse

▸ **nonAnnotatedStringParse**(`value`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `string` |

#### Returns

`string`

#### Defined in

[a/firstTest.ts:13](https://gitlab.com/louis.robinson/typedoc/-/blob/1dcf5dd/packages/analytics-js/src/a/firstTest.ts#L13)

___

### preProcessPhoneNumber

▸ **preProcessPhoneNumber**(`phoneNumber`): `string`

Preprocesses a phone number for use in a LiveRamp ATS envelope. See: https://developers.liveramp.com/authenticatedtraffic-api/docs/3-prepare-the-identifiers.
- Removes leading 1s (US country code), including any leading plus sign (+)
- Removes parentheses, dashes, periods, and spaces

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `phoneNumber` | `string` | The phone number to be preprocessed. |

#### Returns

`string`

- The preprocessed phone number address.

#### Defined in

[a/firstTest.ts:9](https://gitlab.com/louis.robinson/typedoc/-/blob/1dcf5dd/packages/analytics-js/src/a/firstTest.ts#L9)
